# README #

###Platform-agnostic, seed-generated pseudorandom numbers.###

**clsSeed.js** 

requires CryptoJS (for SHA1)
https://code.google.com/p/crypto-js/

**usage**:


```
#!javascript

	var objSeed = new clsSeed(strSeed);
	console.log(objSeed.getSeed());
	console.log(objSeed.getRand());
	console.log(objSeed.getAverage());
```
accepts optional strSeed for reproducible seeding

if strSeed is omitted, a reproducible, timestamp-based
seed will be automatically applied

this rev contains statistical tools - sanity checks that can be reviewed here:
http://webdevtool.net/seed

...

faster production version has no stats data:
http://webdevtool.net/seed/inc/clsSeed.min.js

and can be reviewed here:
http://webdevtool.net/seed/index2.php

----------

**clsSeed.min.js**

Platform-agnostic, seed-generated pseudorandom numbers.

requires CryptoJS (for SHA1)
https://code.google.com/p/crypto-js/

**usage**:

```
#!javascript

	var objSeed = new clsSeed(strSeed);
	console.log(objSeed.getSeed());
	console.log(objSeed.getRand());

```
accepts optional strSeed for reproducible seeding

if strSeed is omitted, a reproducible, timestamp-based
seed will be automatically applied


----------

**jsRandom.js**

example file for additional application usage

requires clsSeed.min.js or clsSeed.js

**usage**:


```
#!javascript

buildRandomArray(); // stages a pre-fetched array of 2000 randoms (reusable pseudorandoms, anyway)

var dblNextRandomInSet = getRandomByDatasetName(strDatasetName); // multiple pointers automatically indexed & incremented on use (reusing the same pseudorandom array)
```