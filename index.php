<!DOCTYPE html>
<html>
<head>

	<title>seeded pseudorandom number generator</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=1" />
	<link rel="stylesheet" type="text/css" href="inc/cssBase.css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha1.js"></script>
	<script src="inc/clsSeed.js"></script>

	<script>

		/* globals */
		var intIterations = 0;
		var objSeed;


		$(function() {
			/* init fluff & defaults*/

			objSeed = new clsSeed($("#txtSeed").val());

			$("#txtSeed").attr("placeholder", "seed (" + objSeed.strDefaultSeed + ")");
			
			intIterations = parseInt("0" + $("#txtIterations").val());
			if(intIterations < 1 || intIterations > 100000){
				$("#txtIterations").val("");
				intIterations = 1000;
			}

			/* output seed data */
			$("#lblSeed").html("Input seed:<br>" + objSeed.getSeed() + "<br><br>Hashed seed data:<br>" + objSeed.strSeedData);
		});

	</script>


    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        /* google visualization biz */
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      
      function drawChart() {
      	
      	var arrData = [
          ['X', 'Y']
        ];

        /* generate the data */
        for(intLoopIndex = 0; intLoopIndex < intIterations; intLoopIndex++){
        	var arrTemp = [objSeed.getRand(), objSeed.getRand()];
        	arrData.push(arrTemp);
        }

        /* google visualization biz */
        var data = google.visualization.arrayToDataTable(arrData);

        var options = {
          	title: 'X / Y scatter (' + intIterations + ' points)',
	        width: 1000,
	        height: 800,
          	hAxis: {minValue: objSeed.dblMin, maxValue: objSeed.dblMax},
          	vAxis: {minValue: objSeed.dblMin, maxValue: objSeed.dblMax},
          	legend: 'none'
        };

        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

        chart.draw(data, options);

        /* outputs */

        if(objSeed.intNaNCount > 0){
	        /* if the algo still manages to div/zero, this will point it out */
        	alert("intNaNCount: " + objSeed.intNaNCount + " Rate: " + (objSeed.intNaNCount/objSeed.intSeedIndex) + " \n\nSee javascript console.");
        }

        /* misnomer now. shows min : avg : max */
        $("#lblAverage").html(
        	"min: " + objSeed.mathRound(objSeed.dblLow) + " , avg: " + 
        	objSeed.getAverage() + " , max: " + 
        	objSeed.mathRound(objSeed.dblHigh)
		);

        /* dump the tab text version for paste into excel */
		$("#txtOutputData").html(arrData.join("\n").replace(/,/gi, "\t"));


		/* do the stats */

      var data2 = new google.visualization.DataTable();
      data2.addColumn('string', 'Group By');
      data2.addColumn('number', 'Hits');

      data2.addRows([
        ['0.0', objSeed.arrStats[0]],
        ['0.1', objSeed.arrStats[1]],
        ['0.2', objSeed.arrStats[2]],
        ['0.3', objSeed.arrStats[3]],
        ['0.4', objSeed.arrStats[4]],
        ['0.5', objSeed.arrStats[5]],
        ['0.6', objSeed.arrStats[6]],
        ['0.7', objSeed.arrStats[7]],
        ['0.8', objSeed.arrStats[8]],
        ['0.9', objSeed.arrStats[9]],
      ]);

      var options2 = {
	    title: 'distribution by tenths',
        width: 1000,
        height: 563,
        hAxis: {
          // title: 'Group By'
        },
        vAxis: {
        	minValue: 0
          // title: 'title here'
        }
      };

      var chart2 = new google.visualization.ColumnChart(
        document.getElementById('chart_div2'));

      chart2.draw(data2, options2);

		if(objSeed.blnGranularStats){

			var arrStatsByKey = objSeed.getGranularByKey(objSeed.objGranularStats);
			var arrStatsByKeyLimit = objSeed.getGranularByKeyLimit(objSeed.objGranularStats);

			var data3 = new google.visualization.DataTable();
			data3.addColumn('string', 'Rand');
			data3.addColumn('number', 'Hits');

			var intSumAll = 0;
			var intCountDupes = 0;
			var intCountAll = 0;
			var intSumDuplicate = 0;

			for(intLoopIndex = 0; intLoopIndex < arrStatsByKey.length; intLoopIndex++){
				intCountAll++;
				intSumAll += arrStatsByKey[intLoopIndex][1];
				if(arrStatsByKey[intLoopIndex][1] > 1){
					intCountDupes++;
					intSumDuplicate += arrStatsByKey[intLoopIndex][1];
				}
			}

			var arrStatsByKeyHavingValue2 = [];

			for(intLoopIndex = 0; intLoopIndex < arrStatsByKeyLimit.length; intLoopIndex++){
				if(arrStatsByKeyLimit[intLoopIndex][1] > 1){
					arrStatsByKeyHavingValue2.push([arrStatsByKeyLimit[intLoopIndex][0],arrStatsByKeyLimit[intLoopIndex][1]]);
				}
			}

			$("#lblRepetitions").html(
				"sum dupes: " + intSumDuplicate + 
				" : count dupes: " + intCountDupes + 
				" : total distinct: " + intCountAll + 
				" : requests: " + objSeed.intSeedIndex + 
				" : dupe rate: " + objSeed.mathRound((intSumDuplicate) / objSeed.intSeedIndex)
			);

			data3.addRows(arrStatsByKeyHavingValue2);

			var options3 = {
				title: 'repetitions of numbers (top ' + arrStatsByKeyHavingValue2.length + ' 2+ hits)',
				width: 1000,
				height: 563,
				hAxis: {
					title: 'Number'
				},
				vAxis: {
					minValue: 0
					// title: 'title here'
				}
			};

			var chart3 = new google.visualization.ColumnChart(
				document.getElementById('chart_div3'));

			chart3.draw(data3, options3);

		}


		$("#objDebugOut").html(objSeed.strDebug);
	}
    </script>

</head>

<body>
	<div id="chart_div"></div>
	<div id="chart_div2"></div>
	<div id="chart_div3"></div>
	<div>
		<p id="lblSeed">&#160;</p>
		<p id="lblAverage">&#160;</p>
		<p id="lblRepetitions">&#160;</p>
		<form action="index.php">
			<p><input type="text" value='<?=$_GET["txtSeed"]?>' id="txtSeed" name="txtSeed"><br><br>
				<input type="text" value='<?=$_GET["txtIterations"]?>' id="txtIterations" name="txtIterations" placeholder="x/y pairs (1000)"></p>
			<p><input type="submit" value="generate"></p>
			<p>
				<textarea id="txtOutputData" cols="10" rows="20"></textarea>
			</p>
		</form>
		<p class="lblMethod">the concept converts input strings to a gibberish data blob, which is parsed
			one character at a time later, each converted to int representation of that byte's ascii code.</p>
		<p class="lblMethod">these numbers are used in as awkward a way possible to generate fractions via division and removal of the integer portion of the result.</p>
	</div>
	<div id="objDebugOut">&#160;</div>
</body>
</html>