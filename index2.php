<!DOCTYPE html>
<html>
<head>

	<title>min seeded pseudorandom number generator</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=1" />
	<link rel="stylesheet" type="text/css" href="inc/cssBase.min.css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha1.js"></script>
	<script src="inc/clsSeed.min.js"></script>

	<script>

		/* globals */
		var intIterations = 0;
		var objSeed;

		$(function() {
			/* init fluff & defaults*/

			objSeed = new clsSeed($("#txtSeed").val());

			$("#txtSeed").attr("placeholder", "seed (" + objSeed.strDefaultSeed + ")");
			
			intIterations = parseInt("0" + $("#txtIterations").val());
			if(intIterations < 1 || intIterations > 100000){
				$("#txtIterations").val("");
				intIterations = 1000;
			}

			/* output seed data */
			$("#lblSeed").html("Input seed:<br>" + objSeed.getSeed() + "<br><br>Hashed seed data:<br>" + objSeed.strSeedData);
		});

	</script>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        /* google visualization biz */
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      
      function drawChart() {
      	
      	var arrData = [
          ['X', 'Y']
        ];

        /* generate the data */
        for(intLoopIndex = 0; intLoopIndex < intIterations; intLoopIndex++){
        	var arrTemp = [objSeed.getRand(), objSeed.getRand()];
        	arrData.push(arrTemp);
        }

        /* google visualization biz */
        var data = google.visualization.arrayToDataTable(arrData);

        var options = {
          	title: 'X / Y scatter (' + intIterations + ' points)',
	        width: 1000,
	        height: 800,
          	hAxis: {minValue: objSeed.dblMin, maxValue: objSeed.dblMax},
          	vAxis: {minValue: objSeed.dblMin, maxValue: objSeed.dblMax},
          	legend: 'none'
        };

        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

        chart.draw(data, options);

        /* outputs */

        /* dump the tab text version for paste into excel */
		$("#txtOutputData").html(arrData.join("\n").replace(/,/gi, "\t"));

	}
    </script>

</head>

<body>
	<div id="chart_div"></div>
	<div>
		<p id="lblSeed">&#160;</p>
		<form action="index2.php">
			<p><input type="text" value='<?=$_GET["txtSeed"]?>' id="txtSeed" name="txtSeed"><br><br>
				<input type="text" value='<?=$_GET["txtIterations"]?>' id="txtIterations" name="txtIterations" placeholder="x/y pairs (1000)"></p>
			<p><input type="submit" value="generate"></p>
			<p>
				<textarea id="txtOutputData" cols="10" rows="20"></textarea>
			</p>
		</form>
		<p class="lblMethod">the concept converts input strings to a gibberish data blob, which is parsed
			one character at a time later, each converted to int representation of that byte's ascii code.</p>
		<p class="lblMethod">these numbers are used in as awkward a way possible to generate fractions via division and removal of the integer portion of the result.</p>
	</div>
</body>
</html>