var objSeed = new clsSeed(strSeed);
var arrRandom = [];
var objRandomPointers = {};
var intRandomCount = 10000;


function buildRandomArray() {
    if (isTablet) {
        intRandomCount = 2000;

    } else if (isMobile) {
        intRandomCount = 500;

    }

    for (intLoopIndex = 0; intLoopIndex < intRandomCount; intLoopIndex++) {
        arrRandom.push(objSeed.getRand());
    }
}

function getRandomByDatasetName(strDatasetName) {
    var dblReturn = 0.00;

    if (strDatasetName in objRandomPointers) {
        objRandomPointers[strDatasetName]++;
        if (objRandomPointers[strDatasetName] >= intRandomCount) {
            objRandomPointers[strDatasetName] = 0;
        }
        dblReturn = arrRandom[objRandomPointers[strDatasetName]];

    } else {
        objRandomPointers[strDatasetName] = 0;
        dblReturn = arrRandom[objRandomPointers[strDatasetName]];
    }

    return dblReturn;
}
